# Quicklizard API Client - Java

Authenticate and perform operations against Quicklizard's REST API from your Java application.

## Installation

Download `target/api-10.-SNAPSHOT.jar` and add to your project lib, or clone and run `mvn package`.

## Usage

### GET Requests

```java
import com.quicklizard.api;

class QLRest {
  private final String API_HOST = "https://rest.quicklizard.com";
  private final String API_KEY = "API_KEY"; //replace with your API key
  private final String API_SECRET = "API_SECRET"; //replace with your API secret
  private Client apiClient = new Client(API_HOST, API_KEY, API_SECRET);
  
  public void doGET() {
    try {
      HttpResponse<String> response = apiClient.get("/api/v3/recommendations/all?client_key=DEMO&page=1&per_page=50");
      System.out.println(response.getBody());
    } catch (URISyntaxException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
```
---
### POST Requests

```java
import com.quicklizard.api;

class QLRest {
  private final String API_HOST = "https://rest.quicklizard.com";
  private final String API_KEY = "API_KEY"; //replace with your API key
  private final String API_SECRET = "API_SECRET"; //replace with your API secret
  private Client apiClient = new Client(API_HOST, API_KEY, API_SECRET);
  
  public void doPOST() {
    try {
      String payload = "{\"payload\":[{\"product_id\": \"abc\"}]}"; //see API docs for complete structure
      HttpResponse<String> response = apiClient.post("/api/v3/products/create", payload);
      System.out.println(response.getBody());
    } catch (URISyntaxException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
```
---
### PUT Requests

```java
import com.quicklizard.api;

class QLRest {
  private final String API_HOST = "https://rest.quicklizard.com";
  private final String API_KEY = "API_KEY"; //replace with your API key
  private final String API_SECRET = "API_SECRET"; //replace with your API secret
  private Client apiClient = new Client(API_HOST, API_KEY, API_SECRET);

  public void doPOST() {
    try {
      String payload = "{\"payload\":[{\"product_id\": \"abc\"}]}"; //see API docs for complete structure
      HttpResponse<String> response = apiClient.put("/api/v3/products/update", payload);
      System.out.println(response.getBody());
    } catch (URISyntaxException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
```
---

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/quicklizard/ql-api-client-java.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

