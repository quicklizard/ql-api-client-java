package com.quicklizard.api;

import junit.framework.TestCase;

public class AuthenticationTest extends TestCase {
  
  private final String API_SECRET = "556f990d4b368184934b59a08d0c6003615ba21d2abdb16fc24ea5966bd747ea";
  private Authentication authenticator = new Authentication(API_SECRET);
  
  public void testGetDigest() {
    String path = "/api/v2/products";
    String queryString = "page=1&qts=123456";
    String expected = "429fcfe520fb0c858b58e92836e9a812e609d3adcd9a46212be4bbcb987ac76f";
    String digest = authenticator.signRequest(path, queryString);
    assertEquals(expected, digest);
  }
}
