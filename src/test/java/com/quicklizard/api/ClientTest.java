package com.quicklizard.api;

import junit.framework.TestCase;

import java.io.Console;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.http.HttpResponse;

public class ClientTest extends TestCase {

  private final String HOST = "https://rest.quicklizard.com";
  private final String API_KEY = "";
  private final String API_SECRET = "";
  private Client apiClient = new Client(HOST, API_KEY, API_SECRET);
  private String jsonPayload = "{\"products\":[{\"product_id\": null}]}";
  private String invalidJSONPayload = "{\"prod\":[{\"product_id\": null}]}";
  
  
  public void testClientGET() {
    try {
      HttpResponse<String> response = apiClient.get("/api/v3/recommendations/all?client_key=demo&per_page=1");
      assertEquals(200, response.statusCode());
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (URISyntaxException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void testClientPOSTWithValidPayload() {
    try {
      HttpResponse<String> response = apiClient.post("/api/v3/products/create?client_key=demo", jsonPayload);
      assertEquals(200, response.statusCode());
    } catch (URISyntaxException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void testClientPUTWithValidPayload() {
    try {
      HttpResponse<String> response = apiClient.put("/api/v3/products/update?client_key=demo", jsonPayload);
      assertEquals(200, response.statusCode());
    } catch (URISyntaxException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void testClientPOSTWithInvalidPayload() {
    try {
      HttpResponse<String> response = apiClient.post("/api/v3/products/create?client_key=demo", invalidJSONPayload);
      assertEquals(500, response.statusCode());
    } catch (URISyntaxException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void testClientPUTWithInvalidPayload() {
    try {
      HttpResponse<String> response = apiClient.put("/api/v3/products/update?client_key=demo", invalidJSONPayload);
      assertEquals(500, response.statusCode());
    } catch (URISyntaxException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}
