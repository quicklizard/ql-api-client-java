package com.quicklizard.api;

import java.security.MessageDigest;

public class Authentication {
  
  private String apiSecret;
  
  /**
   * @constructor
   * @param apiSecret QL API secret
   */
  Authentication(String apiSecret) {
    this.apiSecret = apiSecret;
  }
  
  /**
   * Sign request using path and query string
   * @param path API request path
   * @param queryString API request query-string
   * @return API request signature
   */
  public String signRequest(String path, String queryString) {
    String rawDigest = String.format("%s%s%s", path, queryString, this.apiSecret);
    return getAPIDigest(rawDigest);
  }
  
  /**
   * Sign request using path, query string and request body
   * @param path API request path
   * @param queryString API request query-string
   * @param body API request body
   * @return API request signature
   */
  public String signRequest(String path, String queryString, String body) {
    String rawDigest = String.format("%s%s%s%s", path, queryString, body, this.apiSecret);
    return getAPIDigest(rawDigest);
  }
  
  /**
   * Create API request signature from raw string
   * @param rawDigest raw string to convert to signature
   * @return API signature
   */
  public String getAPIDigest(String rawDigest) {
    try {
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      byte[] hash = md.digest(rawDigest.getBytes("UTF-8"));
      StringBuffer hexDigest = new StringBuffer();
      for (int i = 0; i < hash.length; i++) {
        String hex = Integer.toHexString(0xff & hash[i]);
        if(hex.length() == 1) hexDigest.append('0');
        hexDigest.append(hex);
      }
      return hexDigest.toString();
    } catch(Exception ex){
      throw new RuntimeException(ex);
    }
  }
  
}
