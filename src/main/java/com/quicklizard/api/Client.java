package com.quicklizard.api;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpResponse;
import java.util.Calendar;
import java.util.TimeZone;

public class Client {

  private String host;
  private String key;
  private String secret;
  private Authentication authenticator;
  
  /**
   * @constructor
   * @param host - API host
   * @param apiKey - Your QL API key
   * @param apiSecret - Your QL API secret
   */
  public Client(String host, String apiKey, String apiSecret) {
    this.host = host;
    this.key = apiKey;
    this.secret = apiSecret;
    this.authenticator = new Authentication(this.secret);
  }
  
  /**
   * Perform GET request to QL API
   * @param endpoint - API endpoint. For example `/api/v3/products/enabled?page=1`
   * @return API response
   * @throws URISyntaxException
   * @throws IOException
   * @throws InterruptedException
   */
  public HttpResponse<String> get(String endpoint) throws URISyntaxException, IOException, InterruptedException {
    URI url = getURL(endpoint);
    String digest = this.authenticator.signRequest(url.getPath(), url.getQuery());
    return HTTP.get(url, this.key, digest);
  }
  
  /**
   * Perform POST request to QL API
   * @param endpoint - API endpoint. For example `/api/v3/products/bulk_create`
   * @param payload - API request JSON payload
   * @return API response
   * @throws URISyntaxException
   * @throws IOException
   * @throws InterruptedException
   */
  public HttpResponse<String> post(String endpoint, String payload) throws URISyntaxException, IOException, InterruptedException {
    URI url = getURL(endpoint);
    String digest = this.authenticator.signRequest(url.getPath(), url.getQuery(), payload);
    return HTTP.post(url, payload, this.key, digest);
  }
  
  /**
   * Perform PUT request to QL API
   * @param endpoint - API endpoint. For example `/api/v2/products/bulk_update`
   * @param payload - API request JSON payload
   * @return API response
   * @throws URISyntaxException
   * @throws IOException
   * @throws InterruptedException
   */
  public HttpResponse<String> put(String endpoint, String payload) throws URISyntaxException, IOException, InterruptedException {
    URI url = getURL(endpoint);
    String digest = this.authenticator.signRequest(url.getPath(), url.getQuery(), payload);
    return HTTP.put(url, payload, this.key, digest);
  }
  
  /**
   * Get API request URL from provided endpoint. Joins API host and endpoint and converts into java.net.URL instance
   * @param endpoint - API endpoint. For example `/api/v2/products/bulk_update`
   * @return API request URI
   * @throws URISyntaxException
   */
  private URI getURL(String endpoint) throws URISyntaxException {
    String endpointWithTimestamp = endpointWithTs(endpoint);
    return new URI(String.format("%s%s", this.host, endpointWithTimestamp));
  }

  /**
   * Adds qts timestamp query-string parameter to API endpoint
   * @param endpoint API request endpoint
   * @return API request endpoint with qts parameter
   */
  private String endpointWithTs(String endpoint) {
    TimeZone tz = TimeZone.getTimeZone("UTC");
    Calendar cal = Calendar.getInstance(tz);
    long ts = cal.getTimeInMillis();
    if (endpoint.contains("?")) {
      return String.format("%s&qts=%d", endpoint, ts);
    } else {
      return String.format("%s?qts=%d", endpoint, ts);
    }
  }

}