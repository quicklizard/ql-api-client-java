package com.quicklizard.api;

//import com.mashape.unirest.http.HttpResponse;
//import com.mashape.unirest.http.JsonNode;
//import com.mashape.unirest.http.Unirest;
//import com.mashape.unirest.http.exceptions.UnirestException;
//import com.mashape.unirest.request.GetRequest;
//import com.mashape.unirest.request.HttpRequest;
//import com.mashape.unirest.request.HttpRequestWithBody;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

public class HTTP {

  /**
   * Perform HTTP GET request
   * @param url request url
   * @param key API key
   * @param digest API request signature
   * @return HTTP response
   * @throws URISyntaxException
   * @throws IOException
   * @throws InterruptedException
   */
  public static HttpResponse<String> get(URI url, String key, String digest) throws IOException, InterruptedException {
    HttpClient client = HttpClient.newBuilder()
        .version(HttpClient.Version.HTTP_2)
        .followRedirects(HttpClient.Redirect.NORMAL)
        .build();

    HttpRequest request = java.net.http.HttpRequest.newBuilder()
        .uri(url)
        .GET()
        .header("API_KEY", key)
        .header("API_DIGEST", digest)
        .timeout(Duration.ofSeconds(45))
        .build();

    return client.send(request, HttpResponse.BodyHandlers.ofString());
  }

  /**
   * Perform HTTP POST request
   * @param url request url
   * @param payload request body as JSON
   * @param key API key
   * @param digest API request signature
   * @return HTTP response
   * @throws URISyntaxException
   * @throws IOException
   * @throws InterruptedException
   */
  public static HttpResponse<String> post(URI url, String payload, String key, String digest) throws IOException, InterruptedException {
    HttpClient client = HttpClient.newBuilder()
        .version(HttpClient.Version.HTTP_2)
        .followRedirects(HttpClient.Redirect.NORMAL)
        .build();

    HttpRequest request = java.net.http.HttpRequest.newBuilder()
        .uri(url)
        .POST(HttpRequest.BodyPublishers.ofString(payload))
        .header("Content-Type", "application/json")
        .header("API_KEY", key)
        .header("API_DIGEST", digest)
        .timeout(Duration.ofSeconds(45))
        .build();

    return client.send(request, HttpResponse.BodyHandlers.ofString());
  }

  /**
   * Perform HTTP PUT request
   * @param url request url
   * @param payload request body as JSON
   * @param key API key
   * @param digest API request signature
   * @return HTTP response
   * @throws URISyntaxException
   * @throws IOException
   * @throws InterruptedException
   */
  public static HttpResponse<String> put(URI url, String payload, String key, String digest) throws URISyntaxException, IOException, InterruptedException {
    HttpClient client = HttpClient.newBuilder()
        .version(HttpClient.Version.HTTP_2)
        .followRedirects(HttpClient.Redirect.NORMAL)
        .build();

    HttpRequest request = java.net.http.HttpRequest.newBuilder()
        .uri(url)
        .PUT(HttpRequest.BodyPublishers.ofString(payload))
        .header("Content-Type", "application/json")
        .header("API_KEY", key)
        .header("API_DIGEST", digest)
        .timeout(Duration.ofSeconds(45))
        .build();

    return client.send(request, HttpResponse.BodyHandlers.ofString());
  }
  
}
